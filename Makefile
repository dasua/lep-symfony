#!/bin/bash

UID = $(shell id -u)
APP_BE = app-be
APP_FE = app-fe

help: ## Muestra la ayuda
	@echo 'usage: make [target]'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'

start: ## Start de los contenedores
	@echo 'Iniciando los contenedores'
	@U_ID=${UID} docker-compose up -d

stop: ## Stop de los contenedores
	@eho 'Parando los contenedores'
	@U_ID=${UID} docker-compose stop

down: ## Down de los contenedores
	@echo 'Down de los contenedores'
	@U_ID=${UID} docker-compose stop

restart: ## Reinicio de los contenedores
	@echo 'Reiniciando los contenedores'
	$(MAKE) stop && $(MAKE) start

build: ## Crea las imágenes
	@echo 'Creando las imágenes'
	@U_ID=${UID} docker-compose build

composer-install: ## Instala dependencias de composer
	@echo 'Instalando dependencias'
	@U_ID=${UID} docker exec --user ${UID} ${APP_BE} composer install --no-interaction

ssh-be: ## Ejecuta bash en el contenedor de backend
	@echo 'Entrando en el contenedor de backend'
	@U_ID=${UID} docker exec -it --user ${UID} ${APP_BE} bash

ssh-fe: ## Ejecuta bash en el contenedor de backend con el usuario root
	@echo 'Entrando en el contenedor de frontend con el usuario root'
	@U_ID=${UID} docker exec -it ${APP_FE} bash

code-style: ## Ejecuta php-cs para corregir el estilo del código para seguir las reglas de Symfony (Instalar antes https://cs.symfony.com/)
	@echo 'Corrigiendo estilos del código a las reglas de Symfony'
	@U_ID=${UID} docker exec --user ${UID} ${APP_BE} php-cs-fixer fix src --rules=@Symfony