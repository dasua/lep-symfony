# Linux-nginx-php-symfony

Configuración Docker para iniciar un entorno de desarrollo básico para crear proyetos con Symfony.

## Pero esto que es

Una configuración inicial y básica para poder arrancar un proyecto Symfony desde cero de forma rápida y sencilla
utilizando Docker para gestionar la versión de php, servidor web y servidor de base de datos.

Además, no hace falta que instales nada en tu equipo local ya que está prepadado para poder ejecutar todo desde el
contenedor de php.

## Requisitos

Para poder comenzar a utilizar este stack, debes tener instalado y configurado en tu equipo de desarrollo:

- Ordenador con cualquier **Linux** ya que no lo he probado con otros sistemas operativos (y tampoco me interesa perder
el tiempo con lo que no utilizo).
- **docker**
- **docker-compose**
- **git** (opcional, pero aconsejable tenerlo configurado porque el contenedor utiliza tu configuración de git). En caso
de que no lo tengas, puedes quitar la línea correspondiente asociada al servicio de app-be en el `docker-compose.yml`.
- El usuario con el que ejecutas docker-compose, debe tener el id 1000, en caso contrario, se puede configurar en el 
`docker-compose.yml` ya que está como argumento en `.docker/php/Dockerfile`

## Estructura de los contenedores.

- web: contendor con el servidor web con nginx preconfigurado para Symfony.
- app-be: contenedor con php-fpm (versión 8.0).

### Mirando dentro de la imagen de app-be

- Imagen en la que se basa: [php:8.0-fpm](https://hub.docker.com/_/php/).
- [symfony-cli](https://symfony.com/download).
- [Composer](https://hub.docker.com/_/composer/)
- [mlocati/docker-php-extension-installer](https://github.com/mlocati/docker-php-extension-installer) para la
instalación de extensiones de php.

#### Extensiones instaladas por defecto

- intl
- mcrypt
- opcache
- pdo_pgsql
- zip

En caso de necesitar más, tan sólo has de editar el `.docker/php/Dockerfile` y añadir las que necesites. El listado
lo puedes obtener de [aquí](https://github.com/mlocati/docker-php-extension-installer#supported-php-extensions).

## Prueba de que todo va bien antes de instalar

Lo primero que debes hacer es abrir una terminal y hacer el build de las imáges y arrancar todo.

```shell
docker-compose build && docker-compose up -d
```

Si todo ha ido correctamente, deberías ver algo similar a:

```shell
Creating network "lemp_app" with driver "bridge"
Creating app-be ... done
Creating web   ... done
```

Si es así, puedes entrar con tu navegador a:

_http://localhost:9999_

Y verás el típico

```php
<?php
phpinfo();
```

## Creación de un proyecto nuevo de symfony

Como sabes, para poder crear el proyecto de symfony, el directorio debe estar vacío, así que toca hacer algo de trapi.

Entra en el contenedor y crea el proyecto. Yo lo ejecuto de forma que no inicialice el repositorio git. Si no es tu caso
debes de quitar el flag que sobra.

```shell
docker exec -it app-be bash
usuario@app-be:/app/www/web$ 
```

Como ves, ya te deja con el usuario y ruta donde instalar la aplicación.

```shell
symfony new --no-git --php=8.0 proyecto
```

Si todo ha ido bien, deberías ver algo parecido a esto:

```
* Creating a new Symfony project with Composer
  (running /usr/bin/composer create-project symfony/skeleton /app/www/web/proyecto  --no-interaction)


 [OK] Your project is now ready in /app/www/web/proyecto
```

Y toca traer todo al directorio actual:

```shell
rm -rf public
mv proyecto/* .
mv proyecto/.env .
cat proyecto/.gitignore >> .gitignore && rm proyecto/.gitignore
rmdir proyecto
```

Lo que hace con el .gitignore es añadir lo que tiene el del proyecto de Symfony a lo que hay en el que te has descargado.

Si refrescas la página, verás la página de bienvenida de Symfony.

A partir de este punto, ya es cosa tuya inicializar repositorio, instalación de dependencias, etc.

# Jesús, que no has instalado la base de datos

¿Para qué? En el momento que añades doctrine, flex ya te va a modificar (si le dejas) el docker-compose.yml para
añadir una base de datos de Postres 😇.